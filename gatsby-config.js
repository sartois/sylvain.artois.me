module.exports = {
  siteMetadata: {
    title: `My playground`,
    description: `Sylvain Artois sandbox`,
    keywords: [`Sylvain Artois`, `Weblog`],
    author: `Sylvain Artois`,
    siteUrl: `https://sylvain.artois.io`,
    menuLinks: [
      {
        name: `Code`,
        link: "#",
        type: `internal`,
        subMenu: [
          {
            name: `Computer graphics`,
            link: `/tags/computer-graphics`,
            type: `internal`,
          },
          {
            name: `Computer science`,
            link: `/tags/computer-science`,
            type: `internal`,
          },
        ],
      },
      {
        name: `Contact`,
        link: "/contact",
        type: `internal`,
      },
      {
        name: `(FR) Humanités `,
        link: "/tags/humanities",
        type: `internal`,
      },
    ],
    socialLinks: [
      {
        name: `Email`,
        link: `sylvain@artois.io`,
        location: `footer`, //Options are "all", "header", "footer"
      },
      {
        name: `Twitter`,
        link: `https://twitter.com/sylvainartois`,
        location: `footer`, //Options are "all", "header", "footer"
      },
    ],
  },
  plugins: [
    {
      resolve: `gatsby-plugin-google-fonts`,
      options: {
        fonts: [`Alfa Slab One`, `Libre Baskerville`, `Quattrocento Sans`],
        display: "swap",
      },
    },
    {
      resolve: `gatsby-theme-catalyst-core`,
      options: {
        displaySiteLogo: false,
        displaySiteLogoMobile: false,
        useHero: true,
        useColorMode: false,
        useStickyHeader: true,
        //Default options are:
        // contentPath: `content/pages`,
        // assetPath: `content/assets`,
        // displaySiteTitle: true,
        // displaySiteTitleMobile: false,
        // invertLogo: false,
        // useStickyHeader: false,
        // useSocialLinks: true,
        //footerContentLocation: "left", // "left", "right", "center"
      },
    },
    {
      resolve: `gatsby-theme-catalyst-blog`,
      options: {
        //basePath: `/`,
        //contentPath: `src/content/posts`,
        //assetPath: `src/content/post-images`,
        //excerptLength: `280`,
      },
    },
    `gatsby-theme-catalyst-header-top`, // Try `gatsby-theme-catalyst-header-side` gatsby-theme-catalyst-header-top
    `gatsby-theme-catalyst-footer`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-catalyst`,
        short_name: `catalyst`,
        start_url: `/`,
        background_color: `#ffffff`,
        theme_color: `#cccccc`,
        display: `minimal-ui`,
        icon: `content/assets/catalyst-site-icon.png`, // This path is relative to the root of the site.
      },
    },
  ],
}
