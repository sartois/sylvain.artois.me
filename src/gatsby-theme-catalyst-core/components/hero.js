/** @jsx jsx */
import { jsx } from "theme-ui"
import { useContext } from "react"
import { HomeContext } from "gatsby-theme-catalyst-core"

const HeaderHero = () => {
  const [isHome] = useContext(HomeContext)
  if (!isHome) {
    return null
  } else {
    return null
  }
}

export default HeaderHero
