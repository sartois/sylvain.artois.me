/** @jsx jsx */
import { jsx } from "theme-ui"
import { useContext } from "react"
import { HomeContext } from "gatsby-theme-catalyst-core"
import { useLocation } from "@reach/router"

const SiteMain = ({ children }) => {
  const [isHome] = useContext(HomeContext)

  const location = useLocation()
  const isContact = location.pathname === "/contact"

  return (
    <main
      sx={{
        gridArea: "main",
        variant: "variants.main",
        pt: isHome || isContact ? 0 : "4rem",
      }}
    >
      {children}
    </main>
  )
}

export default SiteMain
