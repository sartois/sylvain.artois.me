/** @jsx jsx */
import { jsx } from "theme-ui"
import { useContext } from "react"
import { HomeContext } from "gatsby-theme-catalyst-core"
import { useLocation } from "@reach/router"

const ContentContainer = ({ children }) => {
  const [isHome] = useContext(HomeContext)
  const location = useLocation()
  const isContact = location.pathname === "/contact"

  return (
    <div
      sx={{
        maxWidth: isHome || isContact ? null : "maxPageWidth",
        background: isHome || isContact ? "#aaaaaa" : null,
        mt: 3,
        mb: 5,
        mx: "auto",
        px: 3,
        variant: "variants.contentContainer",
      }}
    >
      {children}
    </div>
  )
}

export default ContentContainer
