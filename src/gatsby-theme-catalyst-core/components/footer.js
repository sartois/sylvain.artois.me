/** @jsx jsx */
import { jsx } from "theme-ui"
import { useContext } from "react"
import {
  useSiteMetadata,
  useCatalystConfig,
  SocialFooter,
  HomeContext,
} from "gatsby-theme-catalyst-core"
import { useLocation } from "@reach/router"

const SiteFooter = () => {
  const [isHome] = useContext(HomeContext)
  const { title } = useSiteMetadata()
  const { footerContentLocation } = useCatalystConfig()
  const location = useLocation()
  const isContact = location.pathname === "/contact"
  const isLeft = footerContentLocation === "left"
  const isRight = footerContentLocation === "right"
  const isCenter = footerContentLocation === "center"

  const sxStyle = {
    color: "footer.text",
    backgroundColor: "footer.background",
    textAlign:
      (isLeft && "left") || (isRight && "right") || (isCenter && "center"),
    px: 2,
    pt: 2,
    gridArea: "footer",
    a: {
      color: "footer.links",
    },
    variant: "variants.footer",
  }

  if (isContact) {
    sxStyle.position = "absolute"
    sxStyle.bottom = "0"
    sxStyle.width = "100vw"
  }

  return isHome ? null : (
    <footer sx={sxStyle}>
      <div
        sx={{
          width: "100%",
          maxWidth: "maxPageWidth",
          mx: "auto",
          my: 0,
        }}
      >
        <div
          sx={{
            ml: 3,
            a: {
              color: "footer.icons",
              mr: 3,
            },
            "a:last-of-type": {
              mr: 0,
            },
            "a:hover": {
              color: "accent",
            },
            float: "left",
          }}
        >
          <SocialFooter />
        </div>
        <p sx={{ m: 0, mr: 1, fontSize: 0, textAlign: "right" }}>
          Sylvain Artois - {title} - {new Date().getFullYear()}
        </p>
      </div>
    </footer>
  )
}

export default SiteFooter
