/** @jsx jsx */
import { jsx } from "theme-ui"
import { Link } from "gatsby"
import { useContext } from "react"
import { NavContext } from "gatsby-theme-catalyst-core"

const NavMenuLinkInternal = ({ link, hasSubmenu, children }) => {
  const [isNavOpen, setIsNavOpen] = useContext(NavContext) // eslint-disable-line

  return (
    <Link
      to={link}
      activeClassName="active"
      onClick={() => setIsNavOpen(false)}
      aria-haspopup={hasSubmenu}
    >
      {hasSubmenu && <span sx={{ fontSize: "80%" }}> {"\u25BF"}</span>}
      {children}
    </Link>
  )
}

export default NavMenuLinkInternal
