/** @jsx jsx */
import { jsx, useThemeUI } from "theme-ui"
import { useContext } from "react"
import Branding from "gatsby-theme-catalyst-header-top/src/components/branding/branding" // "./branding/branding"
import Nav from "gatsby-theme-catalyst-header-top/src/components/navbar/nav"
import MobileButton from "gatsby-theme-catalyst-header-top/src/components/navbar/nav-mobile-button"
import { NavContext, HomeContext } from "gatsby-theme-catalyst-core"
import { useCatalystConfig } from "gatsby-theme-catalyst-core"

const SiteHeader = () => {
  const [isHome] = useContext(HomeContext)
  const [isNavOpen] = useContext(NavContext)
  const { useStickyHeader } = useCatalystConfig()
  const { theme } = useThemeUI()

  return false ? null : (
    <header
      sx={{
        display: "grid",
        position: useStickyHeader ? "fixed" : "static",
        top: 0,
        width: "100%",
        color: isNavOpen ? "header.textOpen" : "header.text",
        backgroundColor: isNavOpen
          ? "header.backgroundOpen"
          : "header.background",
        gridArea: "header",
        zIndex: "888", // Ensure the header is always on top
        boxShadow: "0px 2px 3px 0px rgba(0,0,39,1)",
        "::after": {
          position: "absolute",
          top: "100%",
          left: "0",
          width: "100%",
          height: "4px",
          backgroundColor: "primary",
          content: "''",
        },
      }}
      id="header"
    >
      <div
        sx={{
          gridRow: "1 / -1",
          gridColumn: "1 / -1",
          alignSelf: "start",
          display: "grid",
          gridTemplateColumns: "auto 1fr",
          gridTemplateRows: [
            theme.sizes.headerHeight + " 1fr",
            null,
            theme.sizes.headerHeight,
            null,
            null,
          ],
          maxWidth: "maxPageWidth",
          width: "100%",
          minHeight: isNavOpen ? "100vh" : "50px",
          m: "0 auto",
          px: [1, null, 3, null, null],
          py: [1, null, 2, null, null],
        }}
      >
        <Branding />
        <Nav />
        <MobileButton />
      </div>
    </header>
  )
}

export default SiteHeader
