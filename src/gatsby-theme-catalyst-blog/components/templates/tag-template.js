/** @jsx jsx */
import { jsx, Styled } from "theme-ui"
import React from "react"
import { Link } from "gatsby"
import Img from "gatsby-image"
import { SEO, Layout } from "gatsby-theme-catalyst-core"
import { Tag } from "../../../components/theme/Tag"
import { PostMeta } from "../../../components/theme/PostMeta"

const TagPage = ({ posts, tag }) => {
  return (
    <Layout>
      <SEO title={"Tag: " + tag} />
      <Styled.h1>
        <Tag tag={tag} />
      </Styled.h1>
      <Styled.ul sx={{ listStyle: "none" }}>
        {posts.map((post, i) => (
          <Styled.li
            key={post.slug}
            sx={{
              display: "flex",
              "@media screen and (max-width: 415px)": {
                flexWrap: "wrap",
              },
              my: 4,
            }}
          >
            {post.featuredImage ? (
              console.log(post.featuredImage) || (
                <>
                  <Styled.div
                    sx={{ order: i % 2 === 0 ? 0 : 1, maxWidth: "50%", p: 4 }}
                  >
                    <img
                      sizes={post.featuredImage.childImageSharp.fluid.sizes}
                      srcset={post.featuredImage.childImageSharp.fluid.srcSet}
                      src={post.featuredImage.childImageSharp.fluid.src}
                      alt={post.title}
                      sx={{ maxWidth: "100%" }}
                    />
                  </Styled.div>
                  <Styled.div
                    sx={{ order: i % 2 === 0 ? 1 : 0, width: "50%", p: 4 }}
                  >
                    <Styled.a as={Link} to={post.slug} sx={{ fontSize: 3 }}>
                      {post.title}
                    </Styled.a>
                    <br />
                    {post.excerpt}
                    <PostMeta post={post} />
                  </Styled.div>
                </>
              )
            ) : (
              <Styled.div />
            )}
          </Styled.li>
        ))}
      </Styled.ul>
    </Layout>
  )
}

export default TagPage
