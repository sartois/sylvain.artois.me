/** @jsx jsx */
import { jsx, Styled } from "theme-ui"
import { useCallback, useState, forwardRef, useMemo } from "react"
import { Link } from "gatsby"
import { SEO, Layout } from "gatsby-theme-catalyst-core"
import { MDXRenderer } from "gatsby-plugin-mdx"
import Img from "gatsby-image"
import { FaRegClock } from "react-icons/fa"
import { Tag } from "../../../components/theme/Tag"
import { extractUrlFromImageCaption } from "../../../modules/contentHelper"
import { PostMeta } from "../../../components/theme/PostMeta"

const PostImage = forwardRef(({ image, altText }, ref) => (
  <Img
    sx={{
      mb: 3,
      variant: "variants.postImage",
    }}
    fluid={image}
    alt={altText}
    ref={ref}
  />
))

const Post = ({ data: { post }, previous, next }) => {
  const [imgHeight, setImgHeight] = useState(1)
  const postIllusCbRef = useCallback(node => {
    if (node !== null) {
      console.log(node.imageRef.current)
      if (node.imageRef.current.offsetWidth > 414)
        setImgHeight(node.imageRef.current.offsetHeight)
      else setImgHeight(1)
    }
  }, [])

  const [imageCaption, imageCaptionUrl] = useMemo(() => {
    const potentialUrl = extractUrlFromImageCaption(post.featuredImageCaption)
    if (potentialUrl) {
      return [
        post.featuredImageCaption.replace(` ${potentialUrl}`, ""),
        potentialUrl,
      ]
    } else {
      return [post.featuredImageCaption, null]
    }
  }, post)

  return (
    <Layout>
      <SEO
        title={post.title}
        description={post.excerpt}
        keywords={post.keywords}
      />

      {post.tags[0] && <Tag tag={post.tags[0]} />}

      <Styled.h1
        sx={{
          textAlign: "center",
          color: "oxfordBlue",
        }}
      >
        {post.title}
      </Styled.h1>

      <PostMeta post={post} />

      {post.featuredImage && (
        <div>
          <PostImage
            image={post.featuredImage.childImageSharp.fluid}
            altText={post.title}
            ref={postIllusCbRef}
          />
          {imageCaptionUrl ? (
            <Styled.p
              sx={{
                my: 1,
                "@media screen and (max-width: 415px)": {
                  display: "none",
                },
              }}
            >
              <Styled.a
                sx={{
                  fontFamily: "navLinks",
                  color: "secondary",
                  fontSize: 1,
                  ":hover": {
                    color: "primary",
                    textDecoration: "none",
                  },
                }}
                href={imageCaptionUrl}
              >
                {imageCaption}
              </Styled.a>
            </Styled.p>
          ) : (
            <Styled.p
              sx={{
                fontFamily: "navLinks",
                color: "secondary",
                fontSize: 1,
                my: 1,
                "@media screen and (max-width: 415px)": {
                  display: "none",
                },
              }}
            >
              {imageCaption}
            </Styled.p>
          )}
        </div>
      )}

      <div
        sx={{
          background: "white",
          maxWidth: "maxContentWidth",
          mx: "auto",
          mt: `-${imgHeight / 2}px`,
          p: 4,
          position: "relative",
          "*:first-child": {
            mt: 0,
          },
        }}
      >
        <MDXRenderer>{post.body}</MDXRenderer>
      </div>

      <div
        sx={{
          mx: "auto",
          my: 4,
          width: "100%",
          display: "flex",
          justifyContent: "space-between",
          "@media screen and (max-width: 415px)": {
            flexWrap: "wrap",
            justifyContent: "center",
          },
        }}
      >
        {previous && (
          <Styled.a
            as={Link}
            to={previous.slug}
            rel="prev"
            sx={{
              fontFamily: "navLinks",
              color: "secondary",
              fontSize: 1,
              ":hover": {
                color: "primary",
                textDecoration: "none",
              },
              px: 3,
            }}
          >
            &#x25C3; {previous.title}
          </Styled.a>
        )}
        {next && (
          <Styled.a
            as={Link}
            to={next.slug}
            rel="next"
            sx={{
              fontFamily: "navLinks",
              color: "secondary",
              fontSize: 1,
              ":hover": {
                color: "primary",
                textDecoration: "none",
              },
              px: 3,
            }}
          >
            {next.title} &#x25B9;
          </Styled.a>
        )}
      </div>
    </Layout>
  )
}

export default Post
