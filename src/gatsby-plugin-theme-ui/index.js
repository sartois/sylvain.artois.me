import { merge } from "theme-ui"
import { BaseTheme } from "gatsby-theme-catalyst-core"

export const mainPalette = {
  oxfordBlue: "#000027",
  slateGray: "#757c89",
  newYorkPink: "#cc786b",
  blanchedAlmond: "#ffe8c2",
  platinum: "#e4e1dd",
}

export default merge(BaseTheme, {
  fonts: {
    siteTitle: '"Alfa Slab One", sans-serif', // Font for main site title
    body: '"Libre Baskerville", serif',
    navLinks: '"Quattrocento Sans", sans-serif', // Font for the nav menu links
    alt: "inherit", //An alternate font style if needed.
    heading: '"Alfa Slab One", sans-serif',
  },
  colors: {
    text: mainPalette.slateGray,
    background: "#ddd",
    primary: mainPalette.newYorkPink,
    secondary: mainPalette.slateGray,
    accent: mainPalette.blanchedAlmond,
    muted: BaseTheme.colors.gray[2],

    ...mainPalette,

    mainPalette,

    header: {
      background: mainPalette.oxfordBlue,
      backgroundOpen: mainPalette.oxfordBlue,
      text: mainPalette.platinum,
      textOpen: mainPalette.platinum,
      icons: mainPalette.platinum,
      iconsHover: mainPalette.blanchedAlmond,
      iconsOpen: mainPalette.platinum,
    },

    footer: {
      background: mainPalette.slateGray,
      text: mainPalette.platinum,
      links: mainPalette.platinum,
      icons: mainPalette.platinum,
    },
  },
  styles: {
    root: {
      ".react-mathjax-preview-result": {
        fontFamily: "navLinks",
      },
      ".gatsby-image-wrapper": {
        mb: 0,
      },
    },
    h1: {
      fontWeight: "normal",
    },
    blockquote: {
      backgroundColor: "primary",
      border: "none",
      fontFamily: "navLinks",
      color: "white",
      "::after": {
        content: "'\"'",
        fontFamily: "heading",
        fontSize: "100px",
        width: "100%",
        height: "40px",
        opacity: ".3",
        display: "block",
        lineHeight: "100px",
        textAlign: "center",
      },
    },
  },
  variants: {
    contentContainer: {
      mt: 0,
      mb: 0,
      px: 0,
    },
    branding: {
      a: {
        span: {
          ":hover": {
            color: mainPalette.blanchedAlmond,
          },
        },
      },
    },
    heading: {
      fontFamily: "heading",
      fontSize: BaseTheme.fontSizes[7],
      color: "primary",
    },
    siteTitle: {
      fontSize: BaseTheme.fontSizes[4],
      fontWeight: "normal",
    },
    navLi: {
      cursor: "pointer",
      fontFamily: "navLinks",
      a: {
        px: 1,
        textDecoration: "none",
        color: "header.text",
        "::after": {
          content: "",
        },
        ":hover, :focus": {
          color: mainPalette.blanchedAlmond,
        },
      },
      ".active": {
        color: mainPalette.blanchedAlmond,
      },
      ".active::after": {
        content: "",
      },
      ul: {
        px: 3,
        a: {
          ":hover": {
            color: mainPalette.blanchedAlmond,
            textDecoration: "none",
          },
        },
      },
    },
    navLinkSubActive: {
      textDecoration: "none",
      color: mainPalette.blanchedAlmond,
    },
    postContainer: {},
    postMeta: {
      color: "black",
      a: {
        color: "black",
      },
    },
    postImage: {
      height: "auto",
      mb: 3,
    },
  },
})
