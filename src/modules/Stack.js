class Stack {
  constructor() {
    this.elements = []
  }
  /**
   * @param {TurtleState} state
   */
  push(state) {
    return this.elements.push(state)
  }
  /**
   * @param  {TurtleState} args
   */
  pop() {
    if (this.elements.length === 0) {
      throw new Error("Stack is empty")
    }
    return this.elements.pop()
  }
}

export default Stack
