export function extractUrlFromImageCaption(imageCaption) {
  if (imageCaption) {
    const potentialUrl = imageCaption.split(" ").pop()
    return potentialUrl.startsWith("http") ? potentialUrl : null
  }
  return null
}
