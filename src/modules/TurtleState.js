class TurtleState {
  constructor({ segmentStart, segmentEnd, lookAt, width, radius }) {
    this.segmentStart = segmentStart
    this.segmentEnd = segmentEnd
    this.lookAt = lookAt
    this.width = width
    this.radius = radius
  }

  clone() {
    return new TurtleState({
      segmentStart: this.segmentStart.clone(),
      segmentEnd: this.segmentEnd.clone(),
      lookAt: this.lookAt.clone(),
      width: this.width,
      radius: this.radius,
    })
  }
}

export default TurtleState
