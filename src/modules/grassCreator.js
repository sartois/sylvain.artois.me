import * as THREE from "three"

/**
 * Inspired by https://github.com/jeromeetienne/threex.grass (2014)
 * But simplified and fully rewrited with modern javascript and ThreeJs
 *
 * Create {positions.length} tufts
 *
 * @param {Array} positions Array of THREE.Vector3
 * @param {Number} tuftSize
 * @returns {THREE.Mesh}
 */
export default function createGrassTufts(
  positions,
  tuftSize,
  textureUrl,
  textureAlphaTest = 0.5
) {
  //tuftsGeometry will holds all tufts
  const tuftsGeometry = new THREE.Geometry()

  const fullTextureUrl = __PATH_PREFIX__ + textureUrl
  const texture = new THREE.TextureLoader().load(fullTextureUrl)

  const tuftsMaterial = new THREE.MeshLambertMaterial({
    map: texture,
    alphaTest: textureAlphaTest,
    side: THREE.DoubleSide,
  })

  positions.forEach(position => {
    const angle = 2 * Math.PI * Math.random()
    const tuftGeom = new THREE.PlaneGeometry(tuftSize, tuftSize)
    //Align tuft on the ground
    tuftGeom.translate(0, tuftSize / 2, 0)
    const tuftMesh = new THREE.Mesh(tuftGeom)
    //Random angle of tufts on x,z plan
    tuftMesh.rotateY(angle)
    tuftMesh.position.copy(position)
    //required for merge to work
    tuftMesh.updateMatrix()
    //perf optim, merge all tufts into one geometry
    tuftsGeometry.merge(tuftMesh.geometry, tuftMesh.matrix)
  })

  const grass = new THREE.Mesh(tuftsGeometry, tuftsMaterial)
  grass.castShadow = true

  return grass
}
