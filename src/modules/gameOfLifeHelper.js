function getRowMajorOffset(x, y, Cols) {
  return x * Cols + y
}

/**
 * Calculate the number of live neighbors
 *
 * @param {array} data
 * @param {int} x
 * @param {int} y
 * @param {int} columnsNb
 */
function countNeighbors(data, x, y, columnsNb) {
  let neighbors = 0
  for (let i = -1; i <= 1; i++) {
    for (let j = -1; j <= 1; j++) {
      let flatMatrixIndex = getRowMajorOffset(x + i, y + j, columnsNb)
      if (data.length < flatMatrixIndex) {
        throw new Error(
          `Indexoutofboundsexception, x: ${x + i}, y:${y + i}, data length: ${
            data.length
          }, flat index: ${flatMatrixIndex}`
        )
      }
      neighbors += data[flatMatrixIndex].state
    }
  }

  // Correct by subtracting the cell state itself.
  neighbors -= data[getRowMajorOffset(x, y, columnsNb)].state

  return neighbors
}

function theRuleOfLife(cellVal, neighborsNb) {
  const Ei = 2
  const Eu = 3
  const Fi = 3
  const Fu = 3

  if (cellVal) {
    return neighborsNb >= Ei && neighborsNb <= Eu ? 1 : 0
  } else {
    return neighborsNb >= Fi && neighborsNb <= Fu ? 1 : 0
  }
}

/**
 * Count the number of live neighbors
 *
 * @param {array} data
 * @param {int} x
 * @param {int} y
 * @param {int} z
 * @param {int} cols
 * @param {int} depth
 *
 */
function countNeighbors3D(data, x, y, z, cols, depth) {
  let neighbors = 0
  for (let i = -1; i <= 1; i++) {
    for (let j = -1; j <= 1; j++) {
      for (let k = -1; k <= 1; k++) {
        let flatMatrixIndex = getRowMajorOffset(
          x + i,
          y + j,
          z + k,
          cols,
          depth
        )
        if (data.length < flatMatrixIndex) {
          throw new Error(
            `Indexoutofboundsexception, x: ${x + i}, y:${y + j}, z:${z +
              k}, data length: ${data.length}, flat index: ${flatMatrixIndex}`
          )
        }
        if (data[flatMatrixIndex]) neighbors++
      }
    }
  }

  // Correct by subtracting the cell state itself.
  if (data[getRowMajorOffset(x, y, z, cols, depth)]) neighbors--

  return neighbors
}

function theRuleOfLife3D(cellVal, neighborsNb) {
  const Ei = 2
  const Eu = 3
  const Fi = 3
  const Fu = 3

  if (cellVal) {
    return neighborsNb >= Ei && neighborsNb <= Eu ? 1 : 0
  } else {
    return neighborsNb >= Fi && neighborsNb <= Fu ? 1 : 0
  }
}

/**
 * See https://dzone.com/articles/memory-layout-of-multi-dimensional-arrays-1
 *
 * @param {int} x
 * @param {int} y
 * @param {int} z
 * @param {int} Cols
 * @param {int} Depth
 */
function getRowMajorOffset3D(x, y, z, Cols, Depth) {
  return z + Depth * (y + Cols * x)
}

function getNextCell(currentState, nextState, age) {
  if (currentState !== nextState) {
    return {
      state: nextState,
      age: 0,
    }
  }

  return {
    state: currentState,
    age: ++age,
  }
}

export { countNeighbors, theRuleOfLife, getRowMajorOffset, getNextCell }
