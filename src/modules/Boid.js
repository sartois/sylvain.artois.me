import { Vector2 } from "three"
import { FlowField } from "./FlowField"

class Boid {
  constructor({
    maxSpeed = 4,
    maxSteerForce = 0.1,
    initialVelocity = new Vector2(0.5 - Math.random(), 0.5 - Math.random()),
    initialPosition = new Vector2(0.5 - Math.random(), 0.5 - Math.random()),
    worldBounds = new Vector2(),
    neighborhoodRadius = 100,
    randomness = 0.6,
  }) {
    this.maxSpeed = maxSpeed
    this.target = null
    this.neighborhoodRadius = neighborhoodRadius
    this.maxSteerForce = maxSteerForce
    this.randomness = randomness

    this.acceleration = new Vector2()
    this.dummy = new Vector2()
    this.steer = new Vector2()
    this.position = initialPosition
    this.velocity = initialVelocity
    this.worldBounds = worldBounds

    this.x = 0
    this.y = 0
  }

  /**
   * @param {Vector2} target
   */
  setGoal(target) {
    this.target = target
  }

  /**
   * @param {Array} boids
   * @param {FlowField} flowFields
   * @returns {Vector2}
   */
  run(boids = [], flowFields = null) {
    if (Math.random() > this.randomness) {
      this._flock(boids)
    }

    this._move(flowFields)

    this.x = this.position.x
    this.y = this.position.y

    return this.position
  }

  _flock(boids) {
    if (this.target) {
      this.acceleration.add(this._reach(this.target, 0.005))
    }

    this.acceleration.add(this._alignment(boids))
    this.acceleration.add(this._cohesion(boids))
    this.acceleration.add(this._separation(boids))
  }

  /**
   * @param {FlowField} flowFields
   */
  _move(flowFields) {
    if (flowFields) {
      this.acceleration.add(flowFields.lookup(this.position))
    }

    this.velocity.add(this.acceleration)
    var l = this.velocity.length()
    if (l > this.maxSpeed) {
      this.velocity.divideScalar(l / this.maxSpeed)
    }

    this.position.add(this.velocity)
    this.acceleration.set(0, 0)

    return this._validatePosition()
  }

  _validatePosition() {
    let { x, y } = this.position
    x = isNaN(x) ? 0 : x
    y = isNaN(y) ? 0 : y
    this.position.set(x, y)
    return this.position.clone()
  }

  _alignment(boids) {
    let boid,
      count = 0,
      distance,
      i
    const velSum = new Vector2()

    for (i = 0; i < boids.length; i++) {
      if (Math.random() > this.randomness) continue
      boid = boids[i]
      distance = boid.position.distanceTo(this.position)
      if (distance > 0 && distance <= this.neighborhoodRadius) {
        velSum.add(boid.velocity)
        count++
      }
    }

    if (count > 0) {
      velSum.divideScalar(count)
      var l = velSum.length()
      if (l > this.maxSteerForce) {
        velSum.divideScalar(l / this.maxSteerForce)
      }
    }

    return velSum
  }

  _cohesion(boids) {
    let count = 0,
      boid,
      distance,
      i
    const posSum = new Vector2()

    for (i = 0; i < boids.length; i++) {
      if (Math.random() > this.randomness) continue

      boid = boids[i]
      distance = boid.position.distanceTo(this.position)

      if (distance > 0 && distance <= this.neighborhoodRadius) {
        posSum.add(boid.position)
        count++
      }
    }

    if (count > 0) {
      posSum.divideScalar(count)
    }

    this.steer.set(0, 0)
    this.steer.subVectors(posSum, this.position)
    var l = this.steer.length()

    if (l > this.maxSteerForce) {
      this.steer.divideScalar(l / this.maxSteerForce)
    }

    return this.steer
  }

  _separation(boids) {
    let boid, distance, i
    const posSum = new Vector2()
    const repulse = new Vector2()

    for (i = 0; i < boids.length; i++) {
      if (Math.random() > this.randomness) continue

      boid = boids[i]
      distance = boid.position.distanceTo(this.position)

      if (distance > 0 && distance <= this.neighborhoodRadius) {
        repulse.subVectors(this.position, boid.position)
        repulse.normalize()
        repulse.divideScalar(distance)
        posSum.add(repulse)
      }
    }

    return posSum
  }

  _reach(target, amount) {
    this.steer.set(0, 0)
    this.steer.subVectors(target, this.position)
    this.steer.multiplyScalar(amount)
    return this.steer
  }
}

export { Boid }
