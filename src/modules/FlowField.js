import { Vector2 } from "three"
import { map } from "./mathHelper"

class FlowField {
  constructor({
    width,
    height,
    resolution = 50,
    edgeRepulsion = true,
    zoneRepulsion = null,
  }) {
    this.resolution = resolution
    this.width = width
    this.height = height
    this.widthMax = Math.round(this.width / 2)
    this.widthMin = -this.widthMax
    this.heightMax = Math.round(this.height / 2)
    this.heightMin = -this.heightMax
    this.cols = Math.ceil(width / resolution)
    this.rows = Math.ceil(height / resolution)
    this.edgeRepulsion = edgeRepulsion
    this.zoneRepulsion = ["nw", "ne", "se", "sw"].some(zone =>
      zone.includes(zoneRepulsion)
    )
      ? zoneRepulsion
      : null
    this.fields = []

    this._init()
  }

  _init() {
    for (let i = 0; i < this.cols; i++) {
      for (let j = 0; j < this.rows; j++) {
        if (!Array.isArray(this.fields[i])) {
          this.fields[i] = []
        }
        this.fields[i][j] = new Vector2()
      }
    }

    if (this.edgeRepulsion) {
      const lastCols = this.cols - 1
      const lastRow = this.rows - 1
      for (let i = 0; i < this.cols; i++) {
        for (let j = 0; j < this.rows; j++) {
          if (i === 0 && j === 0) {
            this.fields[0][0].set(1, -1)
          } else if (i === lastCols && j === 0) {
            this.fields[lastCols][0].set(-1, -1)
          } else if (i === 0 && j === lastRow) {
            this.fields[0][lastRow].set(1, 1)
          } else if (i === lastCols && j === lastRow) {
            this.fields[lastCols][lastRow].set(-1, 1)
          } else if (i === 0) {
            this.fields[0][j].set(1, 0)
          } else if (i === lastCols) {
            this.fields[lastCols][j].set(-1, 0)
          } else if (j === 0) {
            this.fields[i][0].set(0, 1)
          } else if (j === lastRow) {
            this.fields[i][lastRow].set(0, -1)
          }
        }
      }
    }

    if (this.zoneRepulsion) {
      if (this.zoneRepulsion === "sw") {
        const colStart = 1
        const colEnd = Math.ceil(this.cols / 3)
        const rowStart = Math.round(this.rows / 2)
        const rowEnd = this.rows - 1

        for (let ii = colStart; ii < colEnd; ii++) {
          for (let jj = rowStart; jj < rowEnd; jj++) {
            this.fields[ii][jj].set(1, 1)
          }
        }
      }
    }
  }

  lookup(target) {
    if (!target || !target.x || !target.y) {
      console && console.debug("Flowfield.lookup, bad target : ", target)
      return new Vector2()
    }

    const x = map(target.x, this.widthMin, this.widthMax, 0, this.width, true)

    const y = map(
      target.y,
      this.heightMin,
      this.heightMax,
      0,
      this.height,
      true
    )

    const column = Math.floor(x / this.resolution)
    const row = Math.floor(y / this.resolution)

    try {
      return this.fields[column][row].clone()
    } catch (e) {
      console.debug("Error while fetching flowField", this.fields, column, row)
      return new Vector2()
    }
  }
}

export { FlowField }
