import * as THREE from "three"
import React, { useEffect, useState, useMemo, useRef } from "react"
import { Canvas, useThree } from "react-three-fiber"
import { useInterval } from "./shared/useInterval"
import {
  countNeighbors,
  theRuleOfLife,
  getRowMajorOffset,
  getNextCell,
} from "../modules/gameOfLifeHelper"

const colors = ["#CBB384", "#A26234", "#3E291D", "#4386A1", "#9DC5DE"]
const randomThreshold = 0.5

function Content() {
  const {
    size: { width, height },
  } = useThree()

  let rows = Math.round(width / 10) + 2
  let cols = Math.round(height / 10) + 2
  let matrixLength = rows * cols

  const [currentMap, setCurrentMap] = useState([])

  useEffect(() => {
    setCurrentMap(
      Array.from({ length: matrixLength }, () => ({
        state: Math.random() > randomThreshold ? 1 : 0,
        age: 0,
      }))
    )
  }, [])

  const mesh = useRef()
  const dummy = useMemo(() => new THREE.Object3D(), [])
  const emptyCell = useMemo(
    () => ({
      state: 0,
      age: 0,
    }),
    []
  )

  useInterval(() => {
    if (currentMap.length < matrixLength) {
      console.log("CurrentMap not ready")
      return
    }

    const nextMap = new Array(matrixLength)

    for (let x = 0; x < rows; x++) {
      for (let y = 0; y < cols; y++) {
        const currentFlatIndex = getRowMajorOffset(x, y, cols)
        //Skipping edge cells
        if (x > 0 && y > 0 && x !== rows - 1 && y !== cols - 1) {
          const neighbors = countNeighbors(currentMap, x, y, cols)
          const nextState = theRuleOfLife(
            currentMap[currentFlatIndex].state,
            neighbors
          )
          nextMap[currentFlatIndex] = getNextCell(
            currentMap[currentFlatIndex].state,
            nextState,
            currentMap[currentFlatIndex].age
          )
        } else {
          nextMap[currentFlatIndex] = emptyCell
        }
      }
    }

    //Update the map
    setCurrentMap(nextMap)

    const startx = (-(rows - 2) * 10) / 2
    const starty = (-(cols - 2) * 10) / 2

    //Update view
    for (let x = 1; x < rows - 1; x++) {
      for (let y = 1; y < cols - 1; y++) {
        const currentFlatIndex = getRowMajorOffset(x, y, cols)
        if (currentMap[currentFlatIndex].state) {
          dummy.position.set(startx + x * 10, starty + y * 10, 0)
          //dummy.position.set(x * 10, y * 10, 0)
        } else {
          //Very far
          dummy.position.set(startx + x * 10, starty + y * 10, 100)
        }
        dummy.updateMatrix()
        mesh.current.setMatrixAt(currentFlatIndex, dummy.matrix)
      }
    }
    mesh.current.instanceMatrix.needsUpdate = true
  }, 1000)

  return (
    <>
      <instancedMesh ref={mesh} args={[null, null, matrixLength]}>
        <sphereBufferGeometry attach="geometry" args={[5]} />
        <meshStandardMaterial attach="material" color={"blue"} />
      </instancedMesh>
    </>
  )
}

function Lights() {
  return (
    <group>
      <ambientLight intensity={4} />
    </group>
  )
}

function GameOfLife() {
  return (
    <Canvas orthographic>
      <Lights />
      <Content />
    </Canvas>
  )
}

export { GameOfLife }
