import React, { useRef, useMemo, useState, Suspense } from "react"
import * as THREE from "three"
import { Canvas, useFrame, useLoader } from "react-three-fiber"
import { Sky, TrackballControls } from "drei"
import loadable from "@loadable/component"
import useMeasure from "react-use-measure"
import uvChecker from "../../content/assets/textures/1K_UV_checker.jpg"

const MathJax = loadable.lib(() => import("react-mathjax-preview"))

function getUOffset(iy, widthSegments, heightSegments) {
  let uOffset = 0

  if (iy == 0) {
    uOffset = 0.5 / widthSegments
  } else if (iy == heightSegments) {
    uOffset = -0.5 / widthSegments
  }

  return uOffset
}

function getUVsFromNormal(normal) {
  const u = 0.5 + Math.atan2(normal.z, normal.x) / (Math.PI * 2)
  const v = 0.5 - Math.asin(normal.y) / Math.PI

  return [u, v]
}

//Comes from ThreeJs source code https://github.com/mrdoob/three.js/blob/master/src/geometries/SphereGeometry.js
function getSphereData({
  widthSegments,
  heightSegments,
  radius = 1,
  uvMethod = 0,
}) {
  widthSegments = Math.max(3, Math.floor(widthSegments) || 8)
  heightSegments = Math.max(2, Math.floor(heightSegments) || 6)

  //phi
  let longitudeStart = 0
  let longitudeLength = 2 * Math.PI

  //theta
  let colatitudeStart = 0
  let colatitudeLength = Math.PI

  //the four required buffer
  const indices = []
  const vertices = []
  const normals = []
  const uvs = []

  //increase with all iteration. Used to compute AttributeBuffer indices
  let index = 0
  //2D array used to compute AttributeBuffer indices
  const grid = []

  const vertex = new THREE.Vector3()
  const normal = new THREE.Vector3()

  for (let iy = 0; iy <= heightSegments; iy++) {
    const verticesRow = []
    const v = iy / heightSegments

    //allow u special case at pole (for uv mapping)
    const uOffset = getUOffset(iy, widthSegments, heightSegments)

    for (let ix = 0; ix <= widthSegments; ix++) {
      const u = ix / widthSegments

      vertex.x =
        radius *
        Math.cos(longitudeStart + u * longitudeLength) *
        Math.sin(colatitudeStart + v * colatitudeLength)

      vertex.y = radius * Math.cos(colatitudeStart + v * colatitudeLength)

      vertex.z =
        radius *
        Math.sin(longitudeStart + u * longitudeLength) *
        Math.sin(colatitudeStart + v * colatitudeLength)

      vertices.push(vertex.x, vertex.y, vertex.z)

      // normal

      normal.copy(vertex).normalize()
      normals.push(normal.x, normal.y, normal.z)

      // uv
      if (uvMethod === 0) {
        uvs.push(u + uOffset, 1 - v)
      } else if (uvMethod === 1) {
        uvs.push.apply(uvs, getUVsFromNormal(normal.multiplyScalar(-1)))
      } else {
        uvs.push(u, 1 - v)
      }

      verticesRow.push(index++)
    }

    grid.push(verticesRow)
  }

  //get indices for array buffer
  for (let iy = 0; iy < heightSegments; iy++) {
    for (let ix = 0; ix < widthSegments; ix++) {
      const a = grid[iy][ix + 1]
      const b = grid[iy][ix]
      const c = grid[iy + 1][ix]
      const d = grid[iy + 1][ix + 1]

      if (iy !== 0) indices.push(a, b, d)
      if (iy !== heightSegments - 1) indices.push(b, c, d)
    }
  }

  const geometry = new THREE.BufferGeometry()
  geometry.setAttribute(
    "position",
    new THREE.BufferAttribute(new Float32Array(vertices), 3)
  )
  geometry.setAttribute(
    "normal",
    new THREE.BufferAttribute(new Float32Array(normals), 3)
  )
  geometry.setAttribute(
    "uv",
    new THREE.BufferAttribute(new Float32Array(uvs), 2)
  )

  geometry.setIndex(indices)

  return geometry
}

function getBurstSphereGeometry({ widthSegments, heightSegments, radius = 1 }) {
  widthSegments = Math.max(3, Math.floor(widthSegments) || 8)
  heightSegments = Math.max(2, Math.floor(heightSegments) || 6)

  //phi
  let longitudeStart = 0
  let longitudeLength = 2 * Math.PI

  //theta
  let colatitudeStart = 0
  let colatitudeLength = Math.PI

  //only 2 buffers this time
  const vertices = []
  const normals = []

  const northPole = new THREE.Vector3(0, radius, 0)
  const northPoleNormal = new THREE.Vector3()
  northPoleNormal.copy(northPole).normalize()
  const southPole = new THREE.Vector3(0, -radius, 0)
  const southPoleNormal = new THREE.Vector3()
  southPoleNormal.copy(southPole).normalize()
  const vertex1 = new THREE.Vector3()
  const vertex2 = new THREE.Vector3()
  const vertex3 = new THREE.Vector3()
  const vertex4 = new THREE.Vector3()
  const normal1 = new THREE.Vector3()
  const normal2 = new THREE.Vector3()
  const normal3 = new THREE.Vector3()
  const normal4 = new THREE.Vector3()

  for (let iy = 0; iy <= heightSegments - 1; iy++) {
    const v = iy / heightSegments
    const vb = (iy + 1) / heightSegments

    for (let ix = 0; ix <= widthSegments; ix++) {
      const u = ix / widthSegments
      const ub = (ix + 1) / widthSegments

      //pole
      if (iy === 0) {
        //build the pole triangles
        vertex1.x =
          radius *
          Math.cos(longitudeStart + u * longitudeLength) *
          Math.sin(colatitudeStart + vb * colatitudeLength)

        vertex1.y = radius * Math.cos(colatitudeStart + vb * colatitudeLength)

        vertex1.z =
          radius *
          Math.sin(longitudeStart + u * longitudeLength) *
          Math.sin(colatitudeStart + vb * colatitudeLength)

        vertex2.x =
          radius *
          Math.cos(longitudeStart + ub * longitudeLength) *
          Math.sin(colatitudeStart + vb * colatitudeLength)

        vertex2.y = radius * Math.cos(colatitudeStart + vb * colatitudeLength)

        vertex2.z =
          radius *
          Math.sin(longitudeStart + ub * longitudeLength) *
          Math.sin(colatitudeStart + vb * colatitudeLength)

        vertices.push(
          northPole.x,
          northPole.y,
          northPole.z,
          vertex1.x,
          vertex1.y,
          vertex1.z,
          vertex2.x,
          vertex2.y,
          vertex2.z
        )

        // normal

        normal1.copy(vertex1).normalize()
        normal2.copy(vertex2).normalize()
        normals.push(
          northPoleNormal.x,
          northPoleNormal.y,
          northPoleNormal.z,
          normal1.x,
          normal1.y,
          normal1.z,
          normal2.x,
          normal2.y,
          normal2.z
        )
      } else if (iy === heightSegments - 1) {
        vertex1.x =
          radius *
          Math.cos(longitudeStart + u * longitudeLength) *
          Math.sin(colatitudeStart + v * colatitudeLength)

        vertex1.y = radius * Math.cos(colatitudeStart + v * colatitudeLength)

        vertex1.z =
          radius *
          Math.sin(longitudeStart + u * longitudeLength) *
          Math.sin(colatitudeStart + v * colatitudeLength)

        vertex2.x =
          radius *
          Math.cos(longitudeStart + ub * longitudeLength) *
          Math.sin(colatitudeStart + v * colatitudeLength)

        vertex2.y = radius * Math.cos(colatitudeStart + v * colatitudeLength)

        vertex2.z =
          radius *
          Math.sin(longitudeStart + ub * longitudeLength) *
          Math.sin(colatitudeStart + v * colatitudeLength)

        vertices.push(
          vertex1.x,
          vertex1.y,
          vertex1.z,
          southPole.x,
          southPole.y,
          southPole.z,
          vertex2.x,
          vertex2.y,
          vertex2.z
        )

        // normal

        normal1.copy(vertex1).normalize()
        normal2.copy(vertex2).normalize()

        normals.push(
          normal1.x,
          normal1.y,
          normal1.z,
          southPoleNormal.x,
          southPoleNormal.y,
          southPoleNormal.z,
          normal2.x,
          normal2.y,
          normal2.z
        )
      } else {
        vertex1.x =
          radius *
          Math.cos(longitudeStart + u * longitudeLength) *
          Math.sin(colatitudeStart + v * colatitudeLength)

        vertex1.y = radius * Math.cos(colatitudeStart + v * colatitudeLength)

        vertex1.z =
          radius *
          Math.sin(longitudeStart + u * longitudeLength) *
          Math.sin(colatitudeStart + v * colatitudeLength)

        vertex2.x =
          radius *
          Math.cos(longitudeStart + u * longitudeLength) *
          Math.sin(colatitudeStart + vb * colatitudeLength)

        vertex2.y = radius * Math.cos(colatitudeStart + vb * colatitudeLength)

        vertex2.z =
          radius *
          Math.sin(longitudeStart + u * longitudeLength) *
          Math.sin(colatitudeStart + vb * colatitudeLength)

        vertex3.x =
          radius *
          Math.cos(longitudeStart + ub * longitudeLength) *
          Math.sin(colatitudeStart + v * colatitudeLength)

        vertex3.y = radius * Math.cos(colatitudeStart + v * colatitudeLength)

        vertex3.z =
          radius *
          Math.sin(longitudeStart + ub * longitudeLength) *
          Math.sin(colatitudeStart + v * colatitudeLength)

        vertex4.x =
          radius *
          Math.cos(longitudeStart + ub * longitudeLength) *
          Math.sin(colatitudeStart + vb * colatitudeLength)

        vertex4.y = radius * Math.cos(colatitudeStart + vb * colatitudeLength)

        vertex4.z =
          radius *
          Math.sin(longitudeStart + ub * longitudeLength) *
          Math.sin(colatitudeStart + vb * colatitudeLength)

        normal1.copy(vertex1).normalize()
        normal2.copy(vertex2).normalize()
        normal3.copy(vertex3).normalize()
        normal4.copy(vertex4).normalize()

        vertices.push(
          vertex1.x,
          vertex1.y,
          vertex1.z,
          vertex2.x,
          vertex2.y,
          vertex2.z,
          vertex3.x,
          vertex3.y,
          vertex3.z
        )

        normals.push(
          normal1.x,
          normal1.y,
          normal1.z,
          normal2.x,
          normal2.y,
          normal2.z,
          normal3.x,
          normal3.y,
          normal3.z
        )

        vertices.push(
          vertex3.x,
          vertex3.y,
          vertex3.z,
          vertex2.x,
          vertex2.y,
          vertex2.z,
          vertex4.x,
          vertex4.y,
          vertex4.z
        )

        normals.push(
          normal3.x,
          normal3.y,
          normal3.z,
          normal2.x,
          normal2.y,
          normal2.z,
          normal4.x,
          normal4.y,
          normal4.z
        )
      }
    }
  }

  const geometry = new THREE.BufferGeometry()

  const positionAttribute = new THREE.BufferAttribute(
    new Float32Array(vertices),
    3
  )
  positionAttribute.setUsage(THREE.DynamicDrawUsage)
  geometry.setAttribute("position", positionAttribute)
  geometry.setAttribute(
    "normal",
    new THREE.BufferAttribute(new Float32Array(normals), 3)
  )

  return geometry
}

function UVSphere({ widthSegments, heightSegments, radius = 1, uvMethod = 0 }) {
  const mesh = useRef()

  useFrame(() => {
    mesh.current.rotation.x = mesh.current.rotation.y += 0.005
  })

  const geometry = useMemo(() =>
    getSphereData({ widthSegments, heightSegments, radius, uvMethod }, [
      widthSegments,
      heightSegments,
      radius,
      uvMethod,
    ])
  )

  return (
    <mesh ref={mesh} geometry={geometry}>
      <meshBasicMaterial attach="material" color="#df6d14" wireframe />
    </mesh>
  )
}

function TexturedUVSphere({
  widthSegments,
  heightSegments,
  radius = 1,
  uvMethod = 0,
}) {
  const mesh = useRef()

  useFrame(() => {
    mesh.current.rotation.y += 0.005
  })

  const geometry = useMemo(() =>
    getSphereData({ widthSegments, heightSegments, radius, uvMethod }, [
      widthSegments,
      heightSegments,
      radius,
      uvMethod,
    ])
  )

  const [texture] = useLoader(THREE.TextureLoader, [uvChecker])

  return (
    <mesh ref={mesh} rotation-z={-0.15} rotation-x={-0.15} geometry={geometry}>
      <meshStandardMaterial attach="material" map={texture} />
    </mesh>
  )
}

function BurstUVSphere({ widthSegments, heightSegments, radius = 1 }) {
  const mesh = useRef()

  const [hovered, set] = useState()
  const animated = []

  const geometry = useMemo(() =>
    getBurstSphereGeometry({ widthSegments, heightSegments, radius }, [
      widthSegments,
      heightSegments,
      radius,
    ])
  )

  let position

  useFrame((state, delta) => {
    // console.log(delta)
    //mesh.current.rotation.x = mesh.current.rotation.y += 0.005

    if (!hovered) {
      return
    }

    if (!animated.find(el => el.faceIndex === hovered.faceIndex)) {
      const vA = new THREE.Vector3()
      const vB = new THREE.Vector3()
      const vC = new THREE.Vector3()

      const face = hovered.face
      const geometry = hovered.object.geometry
      position = geometry.attributes.position

      vA.fromBufferAttribute(position, face.a)
      vB.fromBufferAttribute(position, face.b)
      vC.fromBufferAttribute(position, face.c)

      animated.push({
        a: vA,
        b: vB,
        c: vC,
        originalPosition: {
          a: vA.clone(),
          b: vB.clone(),
          c: vC.clone(),
        },
        face,
        faceIndex: hovered.faceIndex,
        timeSpan: delta,
      })
    }

    if (animated.length > 0) {
      for (let i = animated.length - 1; i >= 0; i--) {
        const { a, b, c, originalPosition, face, timeSpan } = animated[i]

        animated[i].timeSpan += delta

        //animate
        animated[i].a.multiplyScalar(
          THREE.MathUtils.lerp(1, 2, Math.sin(animated[i].timeSpan * 10))
        )
        animated[i].b.multiplyScalar(
          THREE.MathUtils.lerp(1, 2, Math.sin(animated[i].timeSpan * 10))
        )
        animated[i].c.multiplyScalar(
          THREE.MathUtils.lerp(1, 2, Math.sin(animated[i].timeSpan * 10))
        )

        a.toArray(position.array, face.a)
        b.toArray(position.array, face.b)
        c.toArray(position.array, face.c)

        if (
          a.equals(originalPosition.a) &&
          b.equals(originalPosition.b) &&
          c.equals(originalPosition.c)
        ) {
          animated.splice(i, 1)
        }
      }
      position.needsUpdate = true
    }
  })

  return (
    <mesh
      ref={mesh}
      geometry={geometry}
      onPointerMove={e => {
        console.log(e)
        set(e)
      }}
      onPointerOut={e => set(undefined)}
    >
      {/*<meshBasicMaterial attach="material" color="#df6d14" wireframe />*/}
      <meshPhongMaterial attach="material" color={0xff0000} shininess={100} />
    </mesh>
  )
}

function UVSphereCanvas({ children, debug = false }) {
  const [ref, bounds] = useMeasure()

  return (
    <div ref={ref}>
      <Canvas
        colorManagement
        camera={{ position: [0, 0, 5], fov: 25 }}
        style={{ height: (bounds.width || 200) + "px" }}
      >
        <ambientLight />
        <pointLight position={[10, 0, 5]} />
        <Suspense fallback={null}>{children}</Suspense>
        <Sky />
        <TrackballControls />
        {debug && <axesHelper />}
      </Canvas>
    </div>
  )
}

function Math01() {
  const math01 = String.raw`
\begin{cases}
x&=r \cos \phi \cos \theta \\y&=r \cos \phi \sin \theta \\z&=r \sin \phi 
\end{cases}
`

  return (
    <MathJax fallback={"some math"}>
      {({ default: MathJaxReal }) => <MathJaxReal math={math01} />}
    </MathJax>
  )

  // <MathJax math={math01} />
}

function Math02() {
  const math02 = String.raw`
  \begin{cases}
  x&=r \cos \phi \sin \theta \\z&=r \sin \phi \sin \theta \\y&=r \cos \theta
  \end{cases}
`

  return (
    <MathJax fallback={"some math"}>
      {({ default: MathJaxReal }) => <MathJaxReal math={math02} />}
    </MathJax>
  )
}

function Math03() {
  const math03 = String.raw`
  \begin{cases}
  u&=0.5+{\frac {\operatorname {arctan2} (d_{x},d_{z})}{2\pi }} \\
  v&=0.5-{\frac {\arcsin(d_{y})}{\pi }}
  \end{cases}
`

  return (
    <MathJax fallback={"some math"}>
      {({ default: MathJaxReal }) => <MathJaxReal math={math03} />}
    </MathJax>
  )
}

export {
  UVSphereCanvas as Canvas,
  UVSphere,
  TexturedUVSphere,
  BurstUVSphere,
  Math01,
  Math02,
  Math03,
}
