import React from "react"
import Sketch from "./shared/Sketch"

let input
let img
let canvasSize

function setup(p5, canvasParentRef) {
  canvasSize = canvasParentRef.offsetWidth
  p5.createCanvas(canvasSize, canvasSize).parent(canvasParentRef)
  input = p5.createFileInput(handleFile.bind(null, p5))
  input.parent(canvasParentRef)
  input.position(0, 0)
}

function draw(p5) {
  p5.background(255, 0)
  if (img) {
    drawAndResizeImage(p5)
  }
}

function drawAndResizeImage(p5) {
  let x
  if (img.width > canvasSize) {
    p5.image(img, 0, 0, canvasSize, (canvasSize / img.width) * img.height)
  } else if (img.height > canvasSize) {
    const scaledWidth = (canvasSize / img.height) * img.width
    x = canvasSize - scaledWidth / 2
    p5.image(img, x, 0, img.width, canvasSize)
  } else {
    x = canvasSize - img.width / 2
    p5.image(img, x, 0, img.width, img.height)
  }
}

function handleFile(p5, file) {
  console.log("handleFile", file)
  //print(file)
  if (file.type === "image") {
    img = p5.createImg(file.data, "")
    img.hide()
  } else {
    img = null
  }
}

export default props => {
  console.log("Render sketch")
  return (
    <Sketch
      setup={setup}
      draw={draw}
      style={{ position: "relative" }}
      {...props}
    />
  )
}
