import React, { useRef, useCallback, useMemo } from "react"
import { Box, Flex, Text, Button, useThemeUI } from "theme-ui"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faArrowDown, faExchangeAlt } from "@fortawesome/free-solid-svg-icons"
import anime from "animejs/lib/anime.es.js"

let animation, text, arrayItemSize, parentWidth, theme

const demoArray = [8, 2, 4, 9, 3]

function buildAnimation({ width, height }, totalWidth) {
  const gutter =
    (totalWidth - width * demoArray.length) / (demoArray.length - 1)

  const stepDuration = 3000

  const tl = anime.timeline({
    easing: "easeOutExpo",
    autoplay: false,
    loop: false,
    duration: stepDuration,
    complete: function(anim) {
      text.current.textContent = "The array is now sorted"
    },
  })

  tl.add({
    targets: "#arrayItem1",
    background: theme.colors.accent,
    begin: function(anim) {
      text.current.textContent =
        "Insertion sort start at n1, n0 can't be sorted alone"
    },
  })
  tl.add({
    targets: "#arrayItem0",
    background: theme.colors.info,
    begin: function(anim) {
      text.current.textContent = "Comparre n0 and n1"
    },
  })
  tl.add({
    targets: "#arrayItem1",
    translateX: -width - gutter,
    background: theme.colors.secondary,
    begin: function(anim) {
      text.current.textContent = "n1 is bigger than n0, we must swap"
    },
  })
  tl.add(
    {
      targets: "#arrayItem0",
      translateX: width + gutter,
      background: theme.colors.secondary,
    },
    `-=${stepDuration}`
  )
  tl.add({
    targets: "#arrayItem2",
    background: theme.colors.accent,
    begin: function(anim) {
      text.current.textContent = "The algorithm now looks at n2"
    },
  })
  tl.add({
    targets: "#arrayItem0",
    background: theme.colors.info,
    begin: function(anim) {
      text.current.textContent = "Comparre n2 with n1"
    },
  })
  tl.add({
    targets: "#arrayItem2",
    translateX: -(width + gutter),
    begin: function(anim) {
      text.current.textContent = "n2 is smaller than n1, we must swap"
    },
  })
  tl.add(
    {
      targets: "#arrayItem0",
      translateX: (width + gutter) * 2,
      background: theme.colors.secondary,
    },
    `-=${stepDuration}`
  )

  tl.add({
    targets: "#arrayItem1",
    background: theme.colors.info,
    begin: function(anim) {
      text.current.textContent = "Compare n1 and n0"
    },
  })

  tl.add({
    targets: "#arrayItem2",
    background: theme.colors.secondary,
    begin: function(anim) {
      text.current.textContent = "n1 is bigger than n0, no need to swap"
    },
  })

  tl.add(
    {
      targets: "#arrayItem1",
      background: theme.colors.secondary,
    },
    `-=${stepDuration}`
  )

  tl.add({
    targets: "#arrayItem3",
    background: theme.colors.accent,
    begin: function(anim) {
      text.current.textContent = "The algorithm now looks at n3"
    },
  })

  tl.add({
    targets: "#arrayItem0",
    background: theme.colors.info,
    begin: function(anim) {
      text.current.textContent = "Compare n3 with n2"
    },
  })

  tl.add({
    targets: "#arrayItem3",
    background: theme.colors.secondary,
    begin: function(anim) {
      text.current.textContent = "n3 is bigger than n2, no need to swap"
    },
  })

  tl.add(
    {
      targets: "#arrayItem0",
      background: theme.colors.secondary,
    },
    `-=${stepDuration}`
  )

  tl.add({
    targets: "#arrayItem4",
    background: theme.colors.accent,
    begin: function(anim) {
      text.current.textContent = "The algorithm now looks at n4"
    },
  })

  tl.add({
    targets: "#arrayItem3",
    background: theme.colors.info,
    begin: function(anim) {
      text.current.textContent = "Compare n4 with n3"
    },
  })

  tl.add({
    targets: "#arrayItem4",
    translateX: -(width + gutter),
    begin: function(anim) {
      text.current.textContent = "n4 is smaller than n3, we must swap"
    },
  })

  tl.add(
    {
      targets: "#arrayItem3",
      translateX: width + gutter,
      background: theme.colors.secondary,
    },
    `-=${stepDuration}`
  )

  tl.add({
    targets: "#arrayItem0",
    background: theme.colors.info,
    begin: function(anim) {
      text.current.textContent = "Compare n2 with n3"
    },
  })

  tl.add({
    targets: "#arrayItem4",
    translateX: -(width + gutter) * 2,
    begin: function(anim) {
      text.current.textContent = "n3 is smaller than n2, we must swap"
    },
  })

  tl.add(
    {
      targets: "#arrayItem0",
      translateX: (width + gutter) * 3,
      background: theme.colors.secondary,
    },
    `-=${stepDuration}`
  )

  tl.add({
    targets: "#arrayItem2",
    background: theme.colors.info,
    begin: function(anim) {
      text.current.textContent = "Compare n2 with n1"
    },
  })

  tl.add({
    targets: "#arrayItem4",
    translateX: -(width + gutter) * 3,
    begin: function(anim) {
      text.current.textContent = "n2 is smaller than n1, we must swap"
    },
  })

  tl.add(
    {
      targets: "#arrayItem2",
      translateX: 0,
      background: theme.colors.secondary,
    },
    `-=${stepDuration}`
  )

  tl.add({
    targets: "#arrayItem1",
    background: theme.colors.info,
    begin: function(anim) {
      text.current.textContent = "Compare n1 with n0"
    },
  })

  tl.add({
    targets: "#arrayItem4",
    background: theme.colors.secondary,
    begin: function(anim) {
      text.current.textContent = "n1 is bigger than n0, no need to swap"
    },
  })

  tl.add(
    {
      targets: "#arrayItem1",
      background: theme.colors.secondary,
    },
    `-=${stepDuration}`
  )

  return tl
}

function playAnimation() {
  if (!arrayItemSize || !parentWidth) {
    console.log("I hate react again")
    return
  }
  if (!animation) {
    animation = buildAnimation(arrayItemSize, parentWidth)
  }

  animation.play()
}

function InsertionSortAnimation() {
  text = useRef(null)
  ;({ theme } = useThemeUI())

  const measuredRef = useCallback(node => {
    if (node !== null) {
      arrayItemSize = node.getBoundingClientRect()
    }
  }, [])

  const measuredParentRef = useCallback(node => {
    if (node !== null) {
      parentWidth = node.getBoundingClientRect().width
    }
  }, [])

  return (
    <>
      <Button mb={2} onClick={playAnimation}>
        Play
      </Button>
      <Flex
        sx={{
          justifyContent: "space-between",
        }}
        ref={measuredParentRef}
      >
        {demoArray.map((val, i) => (
          <Box
            p={2}
            key={val}
            id={`arrayItem${i}`}
            color="white"
            bg="secondary"
            sx={{
              width: "15%",
              textAlign: "center",
              borderRadius: "default",
            }}
            ref={measuredRef}
          >
            {val}
          </Box>
        ))}
      </Flex>
      <Box>
        <Text ref={text}>&nbsp;</Text>
      </Box>
    </>
  )
}

export { InsertionSortAnimation }
