import React, { useMemo, Suspense } from "react"
import { Vector3 } from "three"
import { Canvas, useFrame } from "react-three-fiber"
import { Sky, TrackballControls } from "drei"
import useMeasure from "react-use-measure"
import { Controls, useControl } from "react-three-gui"
import { Loading } from "../components/shared/Loading"
import LSystem from "../modules/LSystem"

function LSystemGarden() {
  const n = useControl("Iteration", {
    type: "number",
    value: 7,
    max: 7,
    min: 2,
  })
  const delta = useControl("Angle", {
    type: "number",
    value: 22.5,
    max: 90,
    min: 1,
  })
  const widthDecrease = useControl("Width decrease", {
    type: "number",
    value: 0.9,
    min: 0.1,
  })
  const contractionRatio = useControl("Contraction ratio", {
    type: "number",
    value: 0.7,
    min: 0.1,
  })
  const initialWidth = useControl("Initial width", {
    type: "number",
    value: 1,
    min: 0.1,
  })
  const initialRadius = useControl("Initial radius", {
    type: "number",
    value: 0.1,
    min: 0.1,
  })

  const [ref, bounds] = useMeasure()

  const { leafs, branches } = useMemo(() => {
    const lsystem = new LSystem()
    return lsystem.at(new Vector3()).create({
      n,
      delta,
      widthDecrease,
      contractionRatio,
      initialWidth,
      initialRadius,
      axiom: "A",
      productions: {
        //A: "+[/[&FL!A][^FL!A]]-[[^FL!A][&FL!A]]",
        //A: "F^^[&&A]&&F[&&FA]^^A",
        //A: "F^^[&&A[\\\\F][//F]]&&[&&FA]",
        //A: "F++[&&A[\\\\F][//F]]--[^^A[\\\\F][//F]]",
        A: "F++[&&A[\\\\F][//F]]--[^^A[\\\\F][//F]]",
        F: "FF",
        //F: "SF",
        //S: "FL",
      },
    })
  }, [n, delta, widthDecrease, contractionRatio, initialWidth, initialRadius])

  return (
    <div ref={ref}>
      <Canvas
        camera={{ position: [5, 0.25, 5], fov: 40 }}
        style={{ height: (bounds.width || 200) + "px" }}
      >
        <axesHelper args={[2, 2, 2]} />
        <gridHelper />
        <ambientLight />
        {/*<Plane />*/}
        <directionalLight castShadow color="#fff2b2" />
        <Suspense fallback={Loading}>
          <mesh>
            <primitive attach="geometry" object={leafs} />
            <meshStandardMaterial attach="material" color="#6A7A2F" />
          </mesh>
          <mesh>
            <primitive attach="geometry" object={branches} />
            <meshStandardMaterial attach="material" color="#9A7C60" />
          </mesh>
        </Suspense>
        <Sky />
        <TrackballControls />
      </Canvas>
      <Controls />
    </div>
  )
}

const Plane = () => {
  /*useFrame(({ camera }) => {
    camera.updateMatrixWorld()
    const vector = camera.position.clone()
    vector.applyMatrix4(camera.matrixWorld)
    console.log(vector)
  })*/

  return (
    <mesh rotation={[-Math.PI / 2, 0, 0]} position={[0, -0.5, 0]} receiveShadow>
      <planeBufferGeometry attach="geometry" args={[20, 20]} />
      <meshBasicMaterial attach="material" color="#082444" />
    </mesh>
  )
}

export { LSystemGarden }
