import { VertexNormalsHelper } from "three/examples/jsm/helpers/VertexNormalsHelper"

function addVertexNormalsHelper({ mesh, scene }) {
  let normalsHelper = null

  if (mesh.current && normalsHelper === null) {
    normalsHelper = new VertexNormalsHelper(mesh.current, 0.1, 0xff0000, 1)
    scene.add(normalsHelper)
  }

  return normalsHelper
}

export { addVertexNormalsHelper }
