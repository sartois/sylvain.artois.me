import React, { useEffect, useRef } from "react"
import p5 from "p5"

const noop = () => {}

const p5Events = [
  "draw",
  "windowResized",
  "preload",
  "mouseClicked",
  "doubleClicked",
  "mouseMoved",
  "mousePressed",
  "mouseWheel",
  "mouseDragged",
  "mouseReleased",
  "keyPressed",
  "keyReleased",
  "keyTyped",
  "touchStarted",
  "touchMoved",
  "touchEnded",
  "deviceMoved",
  "deviceTurned",
  "deviceShaken",
]

export default function({ className, style, setup = noop, ...props }) {
  const canvasParentRef = useRef(null)
  let scketch
  useEffect(() => {
    scketch = new p5(p => {
      p.setup = () => {
        setup(p, canvasParentRef.current)
      }

      p5Events.forEach(event => {
        //broken
        if (props[event]) {
          p[event] = () => {
            props[event](p)
          }
        }
      })
    })

    return function cleanup() {
      scketch.remove()
    }
  }, [])

  return (
    <div
      ref={canvasParentRef}
      className={className || "react-p5"}
      style={style || {}}
    />
  )
}
