import React from "react"

function FloatImage({ src, alt, title, style = {} }) {
  const styles = {
    ...style,
    float: "left",
  }

  return <img src={src} style={styles} alt={alt} title={title} />
}

function Clear() {
  return <div style={{ clear: "left" }} />
}

export { FloatImage, Clear }
