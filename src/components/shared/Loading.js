/** @jsx jsx */
import { jsx } from "theme-ui"
import { HTML } from "drei"

function Loading({ color }) {
  return (
    <HTML
      center
      sx={{
        padding: "10px",
        transform: "translate3d(-50%, -50%, 0)",
        color,
      }}
      children="Loading..."
    />
  )
}

export { Loading }
