import React, { useRef, useState } from "react"
import { Canvas, useFrame, useThree } from "react-three-fiber"
// import * as randomNormal from "random-normal"

function RectMesh(props) {
  // This reference will give us direct access to the mesh
  const mesh = useRef()
  const { scene } = useThree()

  // Rotate mesh every frame, this is outside of React without overhead
  useFrame(() => {
    //mesh.current.rotation.x = mesh.current.rotation.y += 0.01
    //console.log(scene)
  })

  return (
    <mesh {...props} ref={mesh} rotation={[-Math.PI / 3, 0, 0]}>
      <planeGeometry attach="geometry" args={[20, 20, 8, 8]} />
      <meshBasicMaterial attach="material" color="hotpink" wireframe />
    </mesh>
  )

  /*var geometry = new THREE.PlaneGeometry(5, 5, 16, 16)
  var material = new THREE.MeshBasicMaterial({
    color: 0xffff00,
    side: THREE.DoubleSide,
  })
  new THREE.Mesh(geometry, material)

  const bounds = [
    vector(-5, 0, -5),
    vector(5, 0, -5),
    vector(50, 50, 0),
    vector(-50, 50, 0),
  ]*/

  /*const geom = new THREE.Geometry()
  geom.vertices.push(bounds[0])
  geom.vertices.push(bounds[1])
  geom.vertices.push(bounds[2])
  geom.faces.push(new THREE.Face3(0, 1, 2))
  geom.computeFaceNormals()

  
  //object.position.z = -100 //move a bit back - size of 500 is a bit big
  //object.rotation.y = -Math.PI * 0.5 //triangle is pointing in depth, rotate it -90 degrees on Y

   return new THREE.Mesh(geom, new THREE.MeshNormalMaterial())*/
}

export default props => {
  return (
    <div style={{ height: "300px" }}>
      <Canvas camera={{ position: [0, 0, 20] }}>
        {/*<ambientLight />*/}
        {/*<pointLight position={[10, 10, 10]} />*/}
        <RectMesh position={[0, 0, 0]} />
      </Canvas>
    </div>
  )
}
