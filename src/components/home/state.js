const state = {
  paragraphs: [
    {
      header: "Computer graphics",
      link: "/tag/computer-graphics",
      text:
        "You might find some useful resources here for you in my notes taken while exploring computer graphics algorithms",
    },
    {
      header: "Computer science",
      link: "/tag/computer-science",
      text: "I also have some articles about code in general",
    },
    {
      header: "Lectures, humeurs, beaux-arts",
      link: "/tag/humanities",
      text:
        "(FR) Trouvez-ici quelques références vers des travaux moins numériques",
    },
    {
      header: "Contact",
      link: "/contact",
      text:
        "I'm an independent software engineer with creative skills, let's get in touch",
    },
  ],
}

export default state
