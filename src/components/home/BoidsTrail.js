import React, { useMemo, useRef, createRef } from "react"
import { useFrame, useThree, extend } from "react-three-fiber"
import { Vector3, Vector2 } from "three"
import * as meshline from "threejs-meshline"
import { Boid } from "../../modules/Boid"
import { getRandomRangeInt, getRandomRange } from "../../modules/mathHelper"
import { FlowField } from "../../modules/FlowField"

extend(meshline)

function BoidsTrail({ theme }) {
  const { viewport } = useThree()
  const boidsCount = useMemo(
    () => Math.round((viewport.width * viewport.height) / 10000),
    [viewport]
  )
  const bounds = useMemo(() => new Vector2(viewport.width, viewport.height), [
    viewport,
  ])

  const coords = useMemo(() => {
    const widthMax = Math.round(viewport.width / 2)
    const widthMin = -widthMax
    const heightMax = Math.round(viewport.height / 2)
    const heightMin = -heightMax
    return {
      widthMin,
      widthMax,
      heightMin,
      heightMax,
    }
  }, [viewport])

  const boids = useMemo(
    () =>
      new Array(boidsCount).fill().map(
        () =>
          new Boid({
            worldBounds: bounds,
            maxSpeed: getRandomRangeInt(2, 8),
            neighborhoodRadius: getRandomRangeInt(25, 150),
            maxSteerForce: getRandomRange(0.05, 0.2),
            randomness: Math.random(),
            initialPosition: new Vector2(
              getRandomRangeInt(coords.widthMin, coords.widthMax),
              getRandomRangeInt(coords.heightMin, coords.heightMax)
            ),
          })
      ),
    [bounds]
  )

  return (
    <Lines colors={Object.values(theme.colors.mainPalette)} boids={boids} />
  )
}

function Lines({ colors, boids }) {
  const meshLineRefs = useRef([])
  const dummy = new Vector3()
  const { viewport } = useThree()
  const resolution = useMemo(
    () => new Vector2(viewport.width, viewport.height),
    [viewport]
  )

  if (meshLineRefs.current.length !== boids.length) {
    // add or remove refs
    meshLineRefs.current = Array(boids.length)
      .fill()
      .map((_, i) => meshLineRefs.current[i] || createRef())
  }

  const flowField = useMemo(
    () =>
      new FlowField({
        width: viewport.width,
        height: viewport.height,
        zoneRepulsion: "sw",
      })
  )

  const lines = useMemo(
    () =>
      console.log("Generate lines") ||
      boids.map((boid, i) => {
        const points = new Array(getRandomRangeInt(10, 50))
          .fill()
          .map(() => boid.position.clone())
        //const curve = new SplineCurve(points).getPoints(20)
        //console.log(`SplineCurve ${i}`, curve, "Points", points)
        return {
          color: colors[parseInt(colors.length * Math.random())],
          width: Math.max(2, 20 * Math.random()),
          curve: points,
        }
      }),
    [colors]
  )

  useFrame(() => {
    /*const qTree = quadtree()
    qTree.addAll(boids)*/

    boids = boids.map((boid, i) => {
      // const closedBoids = qTree.find(boid.x, boid.y, boid.neighborhoodRadius)
      const position = boid.run(boids, flowField)
      meshLineRefs.current[i].current.advance(
        dummy.set(position.x, position.y, 0)
      )
      return boid
    })
  })

  return lines.map(({ curve, width, color }, index) => (
    <mesh key={`meshline_${index}`}>
      <meshLine
        attach="geometry"
        vertices={curve}
        ref={meshLineRefs.current[index]}
      />
      <meshLineMaterial
        attach="material"
        lineWidth={width}
        color={color}
        resolution={resolution}
        sizeAttenuation={1}
      />
    </mesh>
  ))
}

export { BoidsTrail }
