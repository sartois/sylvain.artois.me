/** @jsx jsx */
import { jsx, useThemeUI } from "theme-ui"
import { useSiteMetadata, useCatalystConfig } from "gatsby-theme-catalyst-core"

function HomeFooter() {
  const { title } = useSiteMetadata()
  const { theme } = useThemeUI()

  //iPhone 8+ is 414 px
  const mq =
    typeof window !== "undefined"
      ? window.matchMedia("(min-width: 415px)")
      : {
          matches: false,
        }

  return mq.matches ? (
    <footer
      sx={{
        color: "footer.text",
        backgroundColor: "footer.background",
        textAlign: "right",
        px: theme.space[2],
        a: {
          color: "footer.links",
        },
        variant: "variants.footer",
        boxShadow: "0px 0px 3px 0px rgba(0,0,39,1);",
        position: "absolute",
        bottom: "5px",
        right: "5px",
      }}
    >
      <span
        sx={{
          m: 0,
          fontSize: theme.fontSizes[0],
          fontFamily: theme.fonts.navLinks,
        }}
      >
        Sylvain Artois, {title}, {new Date().getFullYear()}
      </span>
      <span
        sx={{
          mx: theme.space[1],
          fontSize: theme.fontSizes[0],
          fontFamily: theme.fonts.navLinks,
        }}
      >
        -
      </span>
      <span
        sx={{
          m: 0,
          fontSize: theme.fontSizes[0],
          fontFamily: theme.fonts.navLinks,
        }}
      >
        Tribute to Gérard Fromanger
      </span>
    </footer>
  ) : null
}

export { HomeFooter }
