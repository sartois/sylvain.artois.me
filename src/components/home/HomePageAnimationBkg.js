import React, { Suspense } from "react"
import { Canvas } from "react-three-fiber"
import { useThemeUI } from "theme-ui"
import { Loading } from "../shared/Loading"
import { AnimatedBkg } from "./AnimatedBkg"

function HomePageAnimationBkg() {
  const themeUiContext = useThemeUI()
  const { theme } = themeUiContext
  const devicePixelRatio =
    typeof window !== "undefined" ? window.devicePixelRatio : 1

  return (
    <Canvas
      concurrent
      pixelRatio={devicePixelRatio}
      orthographic
      camera={{ position: [0, 0, 500] }}
    >
      <Suspense fallback={<Loading color={theme.colors.secondary} />}>
        <AnimatedBkg theme={theme} />
      </Suspense>
    </Canvas>
  )
}

export { HomePageAnimationBkg }
