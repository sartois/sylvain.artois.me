import React from "react"
import { HomePageAnimationBkg } from "./home/HomePageAnimationBkg"
import { HomeFooter } from "./home/HomeFooter"

function HomePage() {
  return (
    <>
      <div style={{ width: "100%", height: "100vh" }}>
        <HomePageAnimationBkg />
      </div>
      <HomeFooter />
    </>
  )
}

export { HomePage }
