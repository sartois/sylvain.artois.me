/** @jsx jsx */
import { jsx, Styled } from "theme-ui"
import { FaRegClock } from "react-icons/fa"

export function PostMeta({ post: { authorLink, author, date, timeToRead } }) {
  return (
    <Styled.p
      sx={{
        textAlign: "center",
        fontSize: 1,
      }}
    >
      <a
        href={authorLink}
        target="_blank"
        sx={{
          textDecoration: "none",
          color: "secondary",
          ":hover": {
            color: "primary",
          },
        }}
      >
        {author}
      </a>{" "}
      &bull; {date} &bull;{" "}
      <FaRegClock
        sx={{
          position: "relative",
          top: "0.125em",
        }}
      />{" "}
      {timeToRead} Min
    </Styled.p>
  )
}
