/** @jsx jsx */
import { jsx, Styled } from "theme-ui"
import { Link } from "gatsby"
import { TAG_PAGE_PATH_PREFIX } from "../../modules/someConst"

export function Tag({ tag }) {
  return (
    <Styled.p
      sx={{
        textAlign: "center",
        fontSize: 1,
        fontFamily: "navLinks",
        textTransform: "uppercase",
      }}
    >
      <Styled.a
        as={Link}
        to={`/${TAG_PAGE_PATH_PREFIX}/${tag}`}
        sx={{
          color: "secondary",
          ":hover": {
            textDecoration: "none",
            color: "primary",
          },
          "::before, ::after": {
            content: "''",
            width: "50px",
            height: "2px",
            mx: 2,
            backgroundColor: "primary",
            display: "inline-block",
            position: "relative",
            top: "-4px",
          },
        }}
      >
        {tag.replace("-", " ")}
      </Styled.a>
    </Styled.p>
  )
}
