import React, { useMemo, Suspense, useRef, useCallback } from "react"
import { Canvas, useLoader, useFrame } from "react-three-fiber"
import {
  TextureLoader,
  InstancedBufferGeometry,
  BufferAttribute,
  InstancedBufferAttribute,
  LinearFilter,
  RGBFormat,
  Vector2,
} from "three"
import { useThemeUI } from "theme-ui"
import { Loading } from "./shared/Loading"
import "./ContactPagePhotoMaterial"
import parisMetro from "../../content/assets/mine/927232_1389201398002108_400203347_n.jpg"

function ContactPage() {
  const themeUiContext = useThemeUI()
  const { theme } = themeUiContext
  const devicePixelRatio =
    typeof window !== "undefined" ? window.devicePixelRatio : 1

  return (
    <Canvas
      concurrent
      pixelRatio={devicePixelRatio}
      orthographic
      camera={{ position: [0, 0, 500] }}
      style={{ height: "100vh" }}
    >
      <Suspense fallback={<Loading color={theme.colors.secondary} />}>
        <ContactAnimatedBkg />
      </Suspense>
    </Canvas>
  )
}

function ContactAnimatedBkg() {
  const texture = useLoader(TextureLoader, parisMetro)

  const [geometry, textureSize] = useMemo(() => {
    texture.minFilter = LinearFilter
    texture.magFilter = LinearFilter
    texture.format = RGBFormat

    const numPoints = texture.image.width * texture.image.height

    let numVisible = 0
    let threshold = 34
    let originalColors

    const img = texture.image
    const canvas = document.createElement("canvas")
    const ctx = canvas.getContext("2d")
    const textureSize = new Vector2(texture.image.width, texture.image.height)

    canvas.width = texture.image.width
    canvas.height = texture.image.height
    ctx.scale(1, -1)
    ctx.drawImage(img, 0, 0, texture.image.width, texture.image.height * -1)
    const imgData = ctx.getImageData(0, 0, canvas.width, canvas.height)
    originalColors = Float32Array.from(imgData.data)

    for (let i = 0; i < numPoints; i++) {
      if (originalColors[i * 4 + 0] > threshold) numVisible++
    }

    // console.log('numVisible', numVisible, this.numPoints);

    const geometry = new InstancedBufferGeometry()

    // positions
    const positions = new BufferAttribute(new Float32Array(4 * 3), 3)
    positions.setXYZ(0, -0.5, 0.5, 0.0)
    positions.setXYZ(1, 0.5, 0.5, 0.0)
    positions.setXYZ(2, -0.5, -0.5, 0.0)
    positions.setXYZ(3, 0.5, -0.5, 0.0)
    geometry.setAttribute("position", positions)

    // uvs
    const uvs = new BufferAttribute(new Float32Array(4 * 2), 2)
    uvs.setXYZ(0, 0.0, 0.0)
    uvs.setXYZ(1, 1.0, 0.0)
    uvs.setXYZ(2, 0.0, 1.0)
    uvs.setXYZ(3, 1.0, 1.0)
    geometry.setAttribute("uv", uvs)

    // index
    geometry.setIndex(
      new BufferAttribute(new Uint16Array([0, 2, 1, 2, 3, 1]), 1)
    )

    const indices = new Uint16Array(numVisible)
    const offsets = new Float32Array(numVisible * 3)
    const angles = new Float32Array(numVisible)

    for (let i = 0, j = 0; i < numPoints; i++) {
      if (originalColors[i * 4 + 0] <= threshold) continue

      offsets[j * 3 + 0] = i % texture.image.width
      offsets[j * 3 + 1] = Math.floor(i / texture.image.width)

      indices[j] = i

      angles[j] = Math.random() * Math.PI

      j++
    }

    geometry.setAttribute(
      "pindex",
      new InstancedBufferAttribute(indices, 1, false)
    )
    geometry.setAttribute(
      "offset",
      new InstancedBufferAttribute(offsets, 3, false)
    )
    geometry.setAttribute(
      "angle",
      new InstancedBufferAttribute(angles, 1, false)
    )

    return [geometry, textureSize]
  }, [texture])

  const photoMaterial = useRef()

  useFrame(({ clock }) => {
    if (photoMaterial.current) {
      photoMaterial.current.uniforms.uTime.value += clock.getDelta()
    }
  })

  const particulesMesh = useCallback(mesh => {
    if (mesh !== null) {
      mesh.material.uniforms.uSize.value = 1.5
      mesh.material.uniforms.uRandom.value = 2.0
      mesh.material.uniforms.uDepth.value = 4.0
    }
  }, [])

  return (
    <mesh geometry={geometry} ref={particulesMesh}>
      <contactPagePhotoMaterial
        attach="material"
        textureSize={textureSize}
        texture={texture}
        ref={photoMaterial}
      />
    </mesh>
  )
}

export { ContactPage }
