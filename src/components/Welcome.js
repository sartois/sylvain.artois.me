import React from "react"
import { Box, Flex, Image, Text, Divider, Button, Link } from "theme-ui"
import me from "../../content/assets/me.jpg"

function Welcome(props) {
  return (
    <>
      <Flex
        sx={{
          //justifyContent: "space-between",
          flexFlow: "row wrap",
        }}
      >
        <Box
          sx={{
            flex: ["100%", "1", "1"],
          }}
        >
          <Image src={me} />
        </Box>
        <Box
          sx={{
            flex: ["100%", "2", "2"],
          }}
        >
          <Text>Hello !</Text>
          <Text>I'm a software engineer</Text>

          <Text>
            I write code for more than 10 years. I like creative, experimental
            project. I can write Javascript (React, React Native, NodeJs), Java
            (Android, Processing), C# (Unity), and I may remember my PHP. I read
            sometimes some Python and C. Recently I give a deep dive into
            computer graphics with WebGL, ThreeJs and Unity. I can't say if I'm
            a backend or frontend developer. Maybe more something like a
            swiss-knife for you. I like computer science, architecting solution,
            mentoring, and math, even if I'm probably the worse math kiddy on
            earth. I also do oil painting.
          </Text>

          <Divider />

          <Flex>
            <Text>Name:</Text>
            <Text>Age:</Text>
            <Text>Where:</Text>
          </Flex>
          <Flex>
            <Text>Sylvain Artois</Text>
            <Text>40</Text>
            <Text>Paris</Text>
          </Flex>

          <Flex>
            <Button as={Link} href="/cv">
              See my CV
            </Button>
            <Button as={Link} href="">
              Get in touch
            </Button>
          </Flex>
        </Box>
      </Flex>
    </>
  )
}

export { Welcome }
