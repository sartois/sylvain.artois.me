
## test 1
##########


## test 2
##########

# Data
x <- seq(-1, 1, length.out = 100)
y <- seq(-1, 1, length.out = 100)

dat1 <- matrix(NA, 100, 100)

for(xi in 1:100){
  for(yi in 1:100){
    dat1[xi, yi] = 0.5 + (atan2(y[yi], x[xi]) / (pi * 2))
  }}

# Plot

#installed.packages("plot3D")
library(plot3D)

## Call persp3D function
persp(x,y,dat1, theta=45, phi=0,  axes=TRUE,  box=TRUE,
#      r=2, scale=TRUE,
      nticks=10, ticktype="detailed", col="cyan", 
      xlab="X-value", ylab="Y-value", zlab="U", main="U coordinates from vertex normal")
