---
title: Sphere mesh generation 101, UV Sphere texture
date: 2019-07-02
featuredImage: ../assets/pixabay/bubbles-51675_1920.jpg
featuredImageCaption: Image par ArtTower de Pixabay https://pixabay.com/fr/photos/bulles-bulle-l-eau-macro-baisse-51675/
author: Sylvain Artois
authorLink: https://twitter.com/sylvainartois
tags: [computer-graphics]
keywords:
  [
    mesh,
    procedural generation,
    how-to generate a sphere vertices,
    uvsphere,
    uv mapping,
    texture,
    texcoord,
  ]
draft: false
slug: /post/computer-graphics/sphere-mesh-generation-101-uvsphere-texture
---

import {
  Canvas,
  UVSphere,
  TexturedUVSphere,
  BurstUVSphere,
  Math01,
  Math02,
  Math03,
} from "../../src/components/UVSphere"
import { FloatImage, Clear } from "../../src/components/shared/FloatImage"

<SEO title="Sphere mesh generation 101, UV Sphere, uv mapping and texture" />

This is part two of this "tutorial", account along the long road following path to procedural generation

## Add some texture

Let's try to add a texture so we can play with _UV Mapping_. With [react-three-fiber](https://github.com/react-spring/react-three-fiber) it's easy to do.

First our root component needs to allow children to be wrapped with a [Suspense callback](https://reactjs.org/docs/concurrent-mode-suspense.html).

```jsx
function UVSphereCanvas({ children }) {
  //...

  return (
    <Canvas>
      {/* ... */}
      <Suspense fallback={null}>{children}</Suspense>
    </Canvas>
  )
}
```

We also load some [UV Mapping checker texture](https://biocinematics.squarespace.com/resources/) and [_useLoader_](https://github.com/react-spring/react-three-fiber/blob/master/api.md#useloader-experimental) and finally update our material definition.

```jsx
import uvChecker from "../assets/uvChecker.png"

function TexturedUVSphere({ widthSegments, heightSegments }) {
  //... Same as UVSphere

  const [texture] = useLoader(THREE.TextureLoader, [uvChecker])

  return (
    <mesh ref={mesh} geometry={geometry}>
      <meshStandardMaterial attach="material" map={texture} />
    </mesh>
  )
}
```

And here it is, some basic texture mapping:

<Canvas>
  <TexturedUVSphere widthSegments={36} heightSegments={18} />
</Canvas>

## Texture coordinates

That's not crazy... There's a lot of deformation !
But we are testing here some _worse case scenario_, where the camera is on the exact same plane with the equator, and the sphere is scaled in to fill the canvas
If you zoom out, and rotate a bit the sphere looks lot better. No glitch at poles.

So what is **UV Mapping** ?
I could say the delicate art of assigning a 2D coordinate to each 3D vertex in order to lay down an image on a mesh.
We often [use a 3D modeler to edit UV's](https://docs.blender.org/manual/en/latest/editors/uv/introduction.html) so you may don't bother at all.
But if you use _procedural generation_, you'll have to compute UV's, and it could be at the price of some epic math headache ...

There's a lot of litterature on it, you may have a look at [map projection](https://en.wikipedia.org/wiki/Map_projection) in cartography and centuries of research (flattening a globe's surface into a plane is not so different from UVs mapping)

I found these 2 books usefulls, and both address the topic with one chapter :

- [Computer Graphics: Principles and Practice, 3rd edition](https://www.pearson.com/store/p/computer-graphics-principles-and-practice/P100001391980/9780321399526)
- [Fundamentals of Computer Graphics, 4th Edition](https://www.routledge.com/Fundamentals-of-Computer-Graphics/Marschner-Shirley/p/book/9781482229394)

## UV mapping our sphere

Here is how **Three.js** compute UV Mapping :

```jsx
// ...

for (let iy = 0; iy <= heightSegments; iy++) {
  // ...

  const v = iy / heightSegments

  // special case for the poles

  let uOffset = 0

  if (iy == 0) {
    uOffset = 0.5 / widthSegments
  } else if (iy == heightSegments) {
    uOffset = -0.5 / widthSegments
  }

  for (let ix = 0; ix <= widthSegments; ix++) {
    const u = ix / widthSegments

    uvs.push(u + uOffset, 1 - v)
  }
}
```

That's not so different from the code used in the [Opengl sphere tutorial](http://www.songho.ca/opengl/gl_sphere.html)

```cpp
// vertex tex coord (s, t) range between [0, 1]
s = (float)j / sectorCount;
t = (float)i / stackCount;
```

We can easily test some UV Mapping algo with a little refactoring in our _getSphereData_. To test the code above, _getUOffset_ will always return 0 (or whatever, we don't use it. Keeping the code for backward compatibility).

```javascript
function getSphereData({
  widthSegments,
  heightSegments,
  radius = 1,
  uvMethod = 0,
}) {
  // ...

  for (let iy = 0; iy <= heightSegments; iy++) {
    const v = iy / heightSegments

    //allow u special case at pole (for uv mapping)
    const uOffset = getUOffset(uvMethod, iy, widthSegments, heightSegments)

    for (let ix = 0; ix <= widthSegments; ix++) {
      const u = ix / widthSegments

      // uv
      if (uvMethod === 0) {
        uvs.push(u + uOffset, 1 - v)
      } else if (uvMethod === 1) {
        uvs.push(u, 1 - v)
      }
    }
  }

  // ...
}
```

The result is the same.

<Canvas>
  <TexturedUVSphere widthSegments={36} heightSegments={18} uvMethod={4} />
</Canvas>

### Let's try to use _normals_.

**Computer Graphics: Principles and Practice** describe 5 ways to compute UVs

- Linear projection
- Cylindrical
- Spherical (that's what we are using now following _Three.js_)
- Piecewise-linear or piecewise-bilinear on a plane from explicit per-vertex texture coordinates (That's what artist do when they use Blender)
- Normal-vector projection onto a sphere

I don't want to reword and make mistake, so lets quote the book on _normal vector projection_

> The texture coordinates assigned to a point (x, y, z) with unit normal vector n = [nx ny nz] are treated as a function of nx, ny, and nz, either as the angular spherical polar coordinates of the point (nx, ny, nz) (the radial coordinate is always 1), [or by using a cube-map texture indexed by n.]

That's a bit mysterious for me, but I saw something close to an implementation on [Wikipedia, UV Mapping](https://en.wikipedia.org/wiki/UV_mapping) :

<Math03 />

Where d is the unit vector from P to the sphere's origin, AKA, normal inverse. Let's try it.

```javascript
function getSphereData({
  widthSegments,
  heightSegments,
  radius = 1,
  uvMethod = 0,
}) {
  // ...

  for (let iy = 0; iy <= heightSegments; iy++) {
    const v = iy / heightSegments

    //allow u special case at pole (for uv mapping)
    const uOffset = getUOffset(uvMethod, iy, widthSegments, heightSegments)

    for (let ix = 0; ix <= widthSegments; ix++) {
      const u = ix / widthSegments

      // ...

      // normal

      normal.copy(vertex).normalize()
      normals.push(normal.x, normal.y, normal.z)

      // uv
      if (uvMethod === 0) {
        uvs.push(u + uOffset, 1 - v)
      } else if (uvMethod === 1) {
        uvs.push(u, 1 - v)
      } else if (uvMethod === 2) {
        uvs.push.apply(uvs, getUVs(normal.multiplyScalar(-1)))
      }
    }
  }

  // ...
}

function getUVs(normal) {
  const u = 0.5 + Math.atan2(normal.z, normal.x) / (Math.PI * 2)
  const v = 0.5 - Math.asin(normal.y) / Math.PI
  return [u, v]
}
```

We dont get something better, clearly see some texture glitch at the north pole.

<Canvas>
  <TexturedUVSphere widthSegments={36} heightSegments={18} uvMethod={1} />
</Canvas>

I don't know why but I wanted to SEE the formula used to compute the u part. I tried gnuplot but it's easier with R.
So here is the arc tangente 2 varying with the normal.

![U coordinates from vertex normal](../assets/R/U-coordinates-from-vertex-normal.png)

@todo :

- end debugging the glitch at north pole with atan2
- play with manual texture wrapping
- maybe try cylindrical projection

We may spend hours playing with UV mapping algorithms, for now the very simple spherical projection is Ok, let use it.

We now need to understand how Three.js index faces and use buffer indices.
For that let's try to make something pretty and play with triangle animation.
