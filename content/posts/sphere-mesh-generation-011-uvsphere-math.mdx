---
title: Sphere mesh generation 101, UV Sphere math
date: 2019-07-01
featuredImage: ../assets/pixabay/globes-1246245_1920.jpg
featuredImageCaption: Image par Free-Photos de Pixabay https://pixabay.com/fr/photos/globes-sph%C3%A8res-cartes-boule-monde-1246245/
author: Sylvain Artois
authorLink: https://twitter.com/sylvainartois
tags: [computer-graphics]
keywords:
  [mesh, procedural generation, how-to generate a sphere vertices, uvsphere]
draft: false
slug: /post/computer-graphics/sphere-mesh-generation-101-uvsphere-math
---

import {
  Canvas,
  UVSphere,
  TexturedUVSphere,
  BurstUVSphere,
  Math01,
  Math02,
  Math03,
} from "../../src/components/UVSphere"
import { FloatImage, Clear } from "../../src/components/shared/FloatImage"

<SEO title="Sphere mesh generation 101, UV Sphere, generate UV sphere vertices from math" />

If like me you like procedural generation, you'll need to know how to build mesh from code.
Let's go back to basis and play with sphere algorithms, and start at the very beginning, the famous **UV Sphere**, where mesh triangles look like the well known latitude / longitude, and with lot more points near the pole than at the equator.

## Sources

Here are the sources I used to write this report :

- I mainly [read source code for **SphereBufferGeometry** directly from **three.js**](https://github.com/mrdoob/three.js/blob/master/src/geometries/SphereGeometry.js)
- I found this [OpenGL Sphere blog post](http://www.songho.ca/opengl/gl_sphere.html) usefull, with the three main sphere mesh generation algo described
- You may have a look at [Processing sphere source code](https://github.com/processing/processing/blob/master/core/src/processing/core/PGraphics.java#L3150) (Java)

For dynamic mesh generation in **Three.js**, I used :

- [**Three.js** Custom BufferGeometry tutorial](https://threejsfundamentals.org/threejs/lessons/threejs-custom-buffergeometry.html), from [_threejsfundamentals.org_](https://threejsfundamentals.org/)
- From **Three.js** examples, [webgl buffergeometry indexed](https://threejs.org/examples/webgl_buffergeometry_indexed.html)

For the math, I like [the wikipedia french article on spherical coordinate system](https://fr.wikipedia.org/wiki/Coordonn%C3%A9es_sph%C3%A9riques) which clearly explain the different convention used in different discipline.

## Math

For whatever reason &phi; and &theta; are not everywhere the same. So first, a short explanation from [Wikipedia](https://en.wikipedia.org/wiki/Spherical_coordinate_system) :

<FloatImage
  src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/4f/3D_Spherical.svg/240px-3D_Spherical.svg.png"
  alt="Physics way for theta and phi"
/>

Spherical coordinates (r, θ, φ) as commonly used in physics (ISO convention): radial distance r, polar angle θ (theta), and azimuthal angle φ (phi).

<Clear />
<FloatImage
  src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/dc/3D_Spherical_2.svg/240px-3D_Spherical_2.svg.png"
  alt="Math way for theta and phi"
/>

Spherical coordinates (r, θ, φ) as often used in mathematics: radial distance r, azimuthal angle θ, and polar angle φ. The meanings of θ and φ have been swapped compared to the physics convention.

<Clear />

It all begin with a trap...

**Azimuthal angle** may also be called [**colatitude**](https://en.wikipedia.org/wiki/Colatitude), the complementary angle of a given _latitude_, aka the angle from the north pole and the wanted point.
It's easier to work with because it's always positive, varying from 0 (north pole) to &pi; (south pole), whereas the _latitude_ starting at the equator varies from &pi;/2 (north pole) to - &pi;/2 (south pole).

_Three.js_ for its [SphereBufferGeometry](https://threejs.org/docs/#api/en/geometries/SphereBufferGeometry) use **colatitude**, and the physics convention, where &phi; is the angle formed by the x axe and the orthogonal projection of the searched point on the equator (the longitude), and &theta; the colatitude.  

In the [OpenGL Sphere tutorial](http://www.songho.ca/opengl/gl_sphere.html), it uses latitude and call-it &phi;, longitude is &theta;. and Z goes up.
So in that case, to get the cartesian coordinate of a point on a sphere, we use the math:

<Math01 />

In _Three.js_, with **Azimuthal angle** and Y going up :

<Math02 />

## Normals

Normals on a sphere is the direction coming from the center and reaching the point, so we only need to normalize the coord vector (and only if radius is not 1, if yes the normal is really the point vector).

We'll talk about UV mapping and face indices building later

## The source code

Lets build a [BufferGeometry](https://threejs.org/docs/#api/en/core/BufferGeometry) with the sphere mathematics we knows (Everything related to UV and faces index is removed for brevity.)

```javascript
//Comes from Three.js source code
//https://github.com/mrdoob/three.js/blob/master/src/geometries/SphereGeometry.js
function getSphereData({ widthSegments = 8, heightSegments = 6, radius = 1 }) {
  widthSegments = Math.max(3, Math.floor(widthSegments))
  heightSegments = Math.max(2, Math.floor(heightSegments))

  //phi
  let longitudeStart = 0
  let longitudeLength = 2 * Math.PI

  //theta
  let colatitudeStart = 0
  let colatitudeLength = Math.PI

  //the four required buffer
  const vertices = []
  const normals = []
  //we'll talk these two later, but you'll need it
  const indices = []
  const uvs = []

  const vertex = new THREE.Vector3()
  const normal = new THREE.Vector3()

  for (let iy = 0; iy <= heightSegments; iy++) {
    const v = iy / heightSegments

    for (let ix = 0; ix <= widthSegments; ix++) {
      const u = ix / widthSegments

      vertex.x =
        radius *
        Math.cos(longitudeStart + u * longitudeLength) *
        Math.sin(colatitudeStart + v * colatitudeLength)

      vertex.y = radius * Math.cos(colatitudeStart + v * colatitudeLength)

      vertex.z =
        radius *
        Math.sin(longitudeStart + u * longitudeLength) *
        Math.sin(colatitudeStart + v * colatitudeLength)

      vertices.push(vertex.x, vertex.y, vertex.z)

      // normal

      normal.copy(vertex).normalize()
      normals.push(normal.x, normal.y, normal.z)

      // @todo UV Mapping
    }
  }

  // @todo, build indices for array buffer

  const geometry = new THREE.BufferGeometry()
  geometry.setAttribute(
    "position",
    new THREE.BufferAttribute(new Float32Array(vertices), 3)
  )
  geometry.setAttribute(
    "normal",
    new THREE.BufferAttribute(new Float32Array(normals), 3)
  )
  geometry.setAttribute(
    "uv",
    new THREE.BufferAttribute(new Float32Array(uvs), 2)
  )

  geometry.setIndex(indices)

  return geometry
}
```

We may use it in react with [react-three-fiber](https://github.com/react-spring/react-three-fiber) :

```jsx
function UVSphere({ widthSegments, heightSegments }) {
  const mesh = useRef()
  //Slowly rotate x and y so we can see poles
  useFrame(() => (mesh.current.rotation.x = mesh.current.rotation.y += 0.005))

  const geometry = useMemo(() =>
    getSphereData({ widthSegments, heightSegments }, [
      widthSegments,
      heightSegments,
    ])
  )

  return (
    <mesh ref={mesh} geometry={geometry}>
      <meshBasicMaterial attach="material" color="#0B6623" wireframe />
    </mesh>
  )
}

function UVSphereCanvas() {
  return (
    <Canvas>
      <ambientLight />
      <pointLight position={[10, 10, 10]} />
      <UVSphere widthSegments={36} heightSegments={18} />
      <Sky />
      <TrackballControls />
    </Canvas>
  )
}
```

We may get something like that :

<Canvas>
  <UVSphere widthSegments={36} heightSegments={18} />
</Canvas>

So we have a base to play with. In the next part, we'll use texture and compute UV mapping.
